<?php

/**
 * Implementation of hook_drush_command().
 */
function regonline_api_drush_command() {
  return array(
    'reg-test' => array(
      'callback' => '_regonline_api_drush_test',
    ),
  );
}

function _regonline_api_drush_test() {
  $args = func_get_args();

  $account_id = variable_get('regonline_account_id', ''); //;
  $username = variable_get('regonline_account_username', ''); //'';
  $password = variable_get('regonline_account_password', ''); ///'  ';
  $event_id = '898354';
  $customer_id = 27454685;
  $customer_username = 'Ben Jeavons';
  $customer_password = 'password';
  $registration_id = 27600347;
  $report_id = 516075;

  include_once('regonline_api.class.inc');
  $regonline = new RegOnline($username, $password, $account_id);
  $regonline->setLogLevel(WATCHDOG_DEBUG);
  //$out = $regonline->getRegistrations($event_id);
  $out = $regonline->getReport($report_id, $event_id);
  //$out = $regonline->getEventFields($event_id);
  //$regs = $regonline->getEvents();
  //$out = $regonline->getRegistration(27610062, $event_id);

  //$out = utf8_decode($out);
  //$out = base64_decode($out);
  //$out = gzuncompress($out);
  print_r($out);
  
  //$out = base64_decode($out);
  //$out = gzinflate($out);
  //$out = utf8_decode($out);
}
