<?php

/**
 *
 */
class RegOnline {
  protected $username;
  protected $password;
  protected $account_id;
  
  protected $base_url = "https://www.regonline.com";
  
  protected $log_level = WATCHDOG_INFO;
  
  public function __construct($username = NULL, $password = NULL, $account_id = NULL) {
    // Establish our own error reporting.
    libxml_use_internal_errors(TRUE);
    
    $this->username = is_null($username) ? variable_get('regonline_account_username', '') : $username;
    $this->password = is_null($password) ? variable_get('regonline_account_password', '') : $password;
    $this->account_id = is_null($account_id) ? (int) variable_get('regonline_account_id', '') : (int) $account_id;
  }
  
  public function setLogLevel($level) {
    $this->log_level = $level;
  }
  
  /**
   * Get events.
   */
  public function getEvents() {
    $op = "/webservices/getEvents.asmx/ByAccountID";
    $params = array(
      'Username' => $this->username,
      'Password' => $this->password,
      'AccountID' => $this->account_id,
    );
    
    try {
      $xml_string = $this->request($op, $params);
      $xml = simplexml_load_string($xml_string);
      // Leave as XML or remake to array? @todo
      return $xml;
    }
    catch (Exception $error) {
      $message = $error->getMessage();
      watchdog('regonline', 'Error in getEvents for !params: !message', array('!params' => print_r($params, TRUE), '!message' => $message), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  /**
   * Get registrations.
   */
  public function getRegistrations($event_id = NULL) {
    $op = "/webservices/geteventregistrations.asmx/RetrieveRegistrationInfo";
    if (is_null($event_id)) {
      $event_id = $this->event_id;
    }
    $params = array(
      'login' => $this->username,
      'password' => $this->password,
      'eventID' => $event_id,
    );
    
    try {
      $xml_string = $this->request($op, $params);
      $xml = simplexml_load_string($xml_string);
      // Leave as XML or remake to array? @todo
      return $xml;
    }
    catch (Exception $error) {
      $message = $error->getMessage();
      watchdog('regonline', 'Error in getRegistrations for !params: !message', array('!params' => print_r($params, TRUE), '!message' => $message), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  /**
   * Get a single registration.
   */
  public function getRegistration($registration_id, $event_id = NULL) {
    $op = "/webservices/retrievesingleregistration.asmx/RetrieveSingleRegistration";
    if (is_null($event_id)) {
      $event_id = $this->event_id;
    }
    $params = array(
      'customerUserName' => $this->username,
      'customerPassword' => $this->password,
      'eventID' => $event_id,
      'registrationID' => $registration_id
    );
    
    try {
      $data = $this->request($op, $params);
      // Data is encoded @todo
      return $data;
    }
    catch (Exception $error) {
      $message = $error->getMessage();
      watchdog('regonline', 'Error in getRegistration for !params: !message', array('!params' => print_r($params, TRUE), '!message' => $message), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  /**
   * Get a non-compressed custom report.
   *
   * @return SimpleXML object
   */
  public function getReport($report_id, $event_id, $start_date = '1/1/2010', $end_date = '12/30/2011', $add_date = TRUE) {
    $op = '/activeReports/regOnline.asmx/getNonCompressedReport';
    $params = array(
      'login' => $this->username,
      'pass' => $this->password,
      'customerID' => $this->account_id,
      'reportID' => $report_id,
      'eventID' => $event_id,
      'startDate' => $start_date,
      'endDate' => $end_date,
      'bAddDate' => $add_date ? 'true' : 'false',
    );

    try {
      $data = $this->request($op, $params);
      $xml = simplexml_load_string($data);
      // Leave as XML or remake to array? @todo
      return $xml;
    }
    catch (Exception $error) {
      $message = $error->getMessage();
      watchdog('regonline', 'Error in getReport for !params: !message', array('!params' => print_r($params, TRUE), '!message' => $message), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  public function getEventFields($event_id) {
    $op = "/webservices/geteventfields.asmx/RetrieveEventFields";
    $params = array(
      'login' => $this->username,
      'password' => $this->password,
      'eventID' => $event_id,
    );
    
    try {
      $data = $this->request($op, $params);
      $xml = simplexml_load_string($data);
      // Leave as XML or remake to array? @todo
      return $xml;
    }
    catch (Exception $error) {
      $message = $error->getMessage();
      watchdog('regonline', 'Error in getEventFields for !params: !message', array('!params' => print_r($params, TRUE), '!message' => $message), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  public function setCustomFieldStatus($registration_id, $custom_field_id, $status_id) {
    $op = "/webservices/SetCustomFieldResponseStatus.asmx/setStatus";
    $params = array(
      'username' => $this->username,
      'password' => $this->password,
      'registrationID' => $registration_id,
      'customFieldID' => $custom_field_id, // @todo not sure what this is.
      'statusID' => $status_id,
    );
    
    try {
      $data = $this->request($op, $params);
      
      return $data;
    }
    catch (Exception $error) {
      $message = $error->getMessage();
      watchdog('regonline', 'Error in setStatus for !params: !message', array('!params' => print_r($params, TRUE), '!message' => $message), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  protected function request($op, $params = array(), $method = 'POST') {
    $headers = array();
    if ($method == 'POST') {
      $headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }

    $url = $this->base_url . $op;

    // Encode request parameters.
    $data = http_build_query($params, '', '&');
    // Debug.
    if ($this->log_level == WATCHDOG_DEBUG) {
      watchdog('regonline', 'Request: !url !params', array('!url' => $url, '!params' => print_r($params, TRUE)), WATCHDOG_DEBUG);
    }
    // Make request.
    $response = drupal_http_request($url, $headers, $method, $data);
    // Debug.
    if ($this->log_level == WATCHDOG_DEBUG) {
      watchdog('regonline', 'Response: !response', array('!response' => print_r($response, TRUE)), WATCHDOG_DEBUG);
    }
    // Handle response.
    switch ($response->code) {
      case '200':
        return $this->parseResponse($response->data);
      default:
        // @todo The API does not say if other codes are sent
        // I receive 500 for incorrect paramaters.
        $message = $response->error . ' ;' . $response->data;
        throw new Exception($message);
    }
  }
  
  /**
   * RegOnline responses for POST requests are XML with one element, a string.
   * However, the string can be HTML-encoded XML or base 64 encoded zipped XML
   * data. We pass back the string to the caller method for parsing.
   *
   * Example response:
   * <?xml version="1.0" encoding="utf-8"?><string xmlns="http://www.regonline.com/webservices/">[string]</string>
   */
  protected function parseResponse($response) {
    $xml = simplexml_load_string($response);
    if (!$xml) {
      $message = "Failed loading XML.";
      foreach(libxml_get_errors() as $error) {
          $message .= " " . $error->message;
      }
      throw new Exception($message);
    }
    else {
      // RegOnline data is in XML string, so return first string.
      return $xml[0];
    }
  }
}